create database assignment1
use assignment1

create table Worker
(WORKER_Id Int primary key,FIRST_NAME char(25) not null ,LAST_NAME char(25),
SALARY int check(salary >=10000 and salary <=25000) , JOINING_DATE datetime, DEPARTMENT char(25)
check(DEPARTMENT in ('HR','sales','Accts','IT'))

Select *  from Worker
insert into Worker values 
(001,'Prateek','Jaiswal', 15000, '2014-02-20 09:00:00', 'HR'),
(002,'Nilesh','Kumar', 12000, '2015-05-15 06:34:00', 'IT'),
(003,'Aman',' Aswal', 13000, '2016-04-12 09:56:00', 'Accts'),
(004,'Ashutosh','Mishra', 20000, '2017-03-23 08:06:00', 'HR')

--QUERY 1
select FIRST_NAME AS WORKER_NAME from Worker

--QUERY 2
select  UPPER(FIRST_NAME) from Worker

--QUERY 3
select distinct DEPARTMENT from Worker

--QUERY 4
--select SUBSTRING ( FIRST_NAME , 1 , 3) from Worker
select LEFT(FIRST_NAME,3) from Worker

--QUERY 5
SELECT charindex ('a' , FIRST_NAME) from Worker

--QUERY 6
select rtrim(FIRST_NAME) from Worker

--Query 7
select ltrim(DEPARTMENT) from Worker

--QUERY 8
select distinct DEPARTMENT , LEN(DEPARTMENT) from Worker

--QUERY 9
select replace(FIRST_NAME , 'a' , 'A') from worker

--QUERY 10 
select concat(FIRST_NAME,' ' , LAST_NAME) as COMPLETE_NAME from Worker

--Query 11
select * from Worker order by FIRST_NAME asc

--Query 12
 select * from Worker order by FIRST_NAME asc , DEPARTMENT desc

 --Query 13
 select * from Worker where FIRST_NAME in ('Prateek','Jaiswal')

 --Query 14 
  select * from Worker where FIRST_NAME not in ('Prateek','Jaiswal')

  --Query 15
  select * from Worker where DEPARTMENT = 'HR'

  --Query 16
    select * from Worker where FIRST_NAME like '%a%'

	--Query 17
	    select * from Worker where FIRST_NAME like '%a'

		--Query 18
		    select * from Worker where FIRST_NAME like '%h' and len(FIRST_NAME)=6

			--Query 19
			select * from Worker where SALARY between 10000 and 20000 

			--Query 20 
			select * from Worker where JOINING_DATE between '2014-02-01' and '2014-02-28'

			--QUERY 21
			select count(*) from Worker
			where DEPARTMENT='HR'
			select * from Worker

 
